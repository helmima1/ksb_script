import moje_sifrovaci_algoritmy

def is_english(text):
    # Vytvoření seznamu četností písmen v anglickém jazyce
    english_letter_frequency = {
        'e': 12.02, 't': 9.10, 'a': 8.12, 'o': 7.68, 'i': 7.31,
        'n': 6.95, 's': 6.28, 'r': 6.02, 'h': 5.92, 'd': 4.32,
        'l': 3.98, 'u': 2.88, 'c': 2.71, 'm': 2.61, 'f': 2.30,
        'y': 2.11, 'w': 2.09, 'g': 2.03, 'p': 1.82, 'b': 1.49,
        'v': 1.11, 'k': 0.69, 'x': 0.17, 'q': 0.11, 'j': 0.10,
        'z': 0.07
    }

    # Převedení textu na malá písmena a odstranění neabecedních znaků
    cleaned_text = ''.join(c.lower() for c in text if c.isalpha())

    # Výpočet četností písmen v textu
    letter_counts = {letter: cleaned_text.count(letter) for letter in english_letter_frequency}

    # Porovnání četností s anglickými hodnotami
    chi_square_value = sum(((letter_counts[letter] - english_letter_frequency[letter]) ** 2) / english_letter_frequency[letter] for letter in english_letter_frequency)

    # Hranice pro přijetí textu jako anglického mohou být nastaveny experimentálně
    # Nižší hodnota chi-square znamená větší podobnost s anglickým textem
    threshold = 100.0

    return chi_square_value < threshold

# TEST OF is_english FUNCTION
# random_english_text = "thissomerandomtextisjustfortestingpurposesandmaynotmakesenseatallbutitservesapurposeinhelpingyouwithyourtesting"
# random_czech_text = "tohlejenejakynahodnyceskytextkterybynemelsplnovatparametrytohoanglickehodoufamzetobudedobrefungovatzejorehori"
# print(f"Is random_english_test english? {is_english(random_english_text)}")
# print(f"Is random_czech_test english? {is_english(random_czech_text)}")

if __name__ == '__main__':

    encrypted_text = "cwrlkirtowmtawakrkkwfmkbmuwjawimwowrwxrngrijrwtavoaumrjgfarrtlatmkfgmwlrawrrmjlykjrirjfeagfjcrjwlmkxakcfjactbmjluovglajwqgkkwkmwnkalklwwyfwgrkaxxlakfwvx"

    print("---------- PROSTY POSUN ----------")
    for posun in range(0, 27):
        deciphered_text = moje_sifrovaci_algoritmy.prosty_posun(encrypted_text, posun)
        if is_english(deciphered_text):
            print(f"Posun = {posun}: "+deciphered_text)

    print("---------- AFFINNI SIFRA ----------")
    valid_a_for_affine_cipher = [1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25]
    for a in valid_a_for_affine_cipher:
        for b in range(0, 27):
            deciphered_text = moje_sifrovaci_algoritmy.afinni_sifra_decrypt(encrypted_text, a, b)
            if is_english(deciphered_text):
                print(f"A = {a}, B = {b}: " + deciphered_text)

    matrices = [[4, 38], [8, 19], [38, 4], [19, 8]]
    print("---------- PROSTY POSUN -> TRANSPOZICE BEZ HESLA ----------")
    for posun in range(0, 27):
        deciphered_text = moje_sifrovaci_algoritmy.prosty_posun(encrypted_text, posun)
        for matrix in matrices:
            deciphered_text_transposed = moje_sifrovaci_algoritmy.transpozicni_sifra(deciphered_text, matrix[0], matrix[1])
            if is_english(deciphered_text_transposed):
                print(f"Posun = {posun}, matrix {matrix}: " + deciphered_text_transposed)

    print("---------- AFFINNI SIFRA -> TRANSPOZICE BEZ HESLA ----------")
    for a in valid_a_for_affine_cipher:
        for b in range(0, 27):
            deciphered_text = moje_sifrovaci_algoritmy.afinni_sifra_decrypt(encrypted_text, a, b)
            # Spočtení transpoziční šifry bez hesla pro všechny možnosti afinní šifry
            for matrix in matrices:
                deciphered_text_transposed = moje_sifrovaci_algoritmy.transpozicni_sifra(deciphered_text, matrix[0], matrix[1])
                if is_english(deciphered_text_transposed):
                    print(f"A = {a}, B = {b}, matrix {matrix}: " + deciphered_text_transposed)

