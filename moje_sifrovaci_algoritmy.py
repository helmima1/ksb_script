import numpy as np

def pismeno_na_cislo_2(pismeno):
    # Zajistíme, že písmeno je v malých písmenech
    pismeno = pismeno.lower()

    # Zkontrolujeme, jestli vstupní znak je písmeno
    if not pismeno.isalpha() or len(pismeno) != 1:
        raise ValueError("Zadejte jedno písmeno.")

    # Převedeme písmeno na číslo podle pořadí v abecedě (počítáme od 0)
    cislo = ord(pismeno) - ord('a')

    return cislo

def cislo_na_pismeno_2(cislo):
    # Zkontrolujeme, jestli vstupní hodnota je platné číslo
    if not isinstance(cislo, int) or cislo < 0 or cislo > 25:
        raise ValueError("Zadejte platné číslo v rozmezí 0 až 25.")

    # Převedeme číslo na odpovídající písmeno v abecedě (počítáme od 0)
    pismeno = chr(cislo + ord('a'))

    return pismeno

def prosty_posun(text, posun):
    desifrovany_text = ""
    for znak in text:
        poradi_znaku = pismeno_na_cislo_2(znak)
        novy_znak = cislo_na_pismeno_2((poradi_znaku+posun)%26)
        desifrovany_text += novy_znak
    return desifrovany_text

def afinni_sifra_encryption(text, a, b):
    desifrovany_text = ""
    for znak in text:
        poradi_znaku = pismeno_na_cislo_2(znak)

        novy_znak = cislo_na_pismeno_2((a*poradi_znaku + b) % 26)
        desifrovany_text += novy_znak
    return desifrovany_text

def afinni_sifra_decrypt(text, a, b):
    if a == 1:
        a = 1
    elif a == 3:
        a = 9
    elif a == 5:
        a = 21
    elif a == 7:
        a = 15
    elif a == 9:
        a = 3
    elif a == 11:
        a = 19
    elif a == 15:
        a = 7
    elif a == 17:
        a = 23
    elif a == 19:
        a = 11
    elif a == 21:
        a = 5
    elif a == 23:
        a = 17
    elif a == 25:
        a = 25
    else:
        raise ValueError("Spatne zadana hodnota a, a="+a)


    desifrovany_text = ""
    for znak in text:
        poradi_znaku = pismeno_na_cislo_2(znak)

        novy_znak = cislo_na_pismeno_2(((a)*(poradi_znaku - b)) % 26)
        desifrovany_text += novy_znak
    return desifrovany_text

def transpozicni_sifra(text, pocet_radku, pocet_sloupcu):
    # Spočítáme, kolik bloků se vejde do délky textu
    pocet_bloku = len(text) // (pocet_radku * pocet_sloupcu)

    # Vytvoříme si prázdnou matici pro dešifrování
    tabulka = np.array([['' for _ in range(pocet_sloupcu)] for _ in range(pocet_radku)])

    # Zaplníme tabulku textem po řádcích
    index = 0
    for radek in range(pocet_radku):
        for sloupec in range(pocet_sloupcu):
            tabulka[radek, sloupec] = text[index]
            index += 1

    # Dešifrování probíhá čtením textu po sloupcích
    desifrovany_text = ''
    for sloupec in range(pocet_sloupcu):
        for radek in range(pocet_radku):
            desifrovany_text += tabulka[radek][sloupec]

    return desifrovany_text

if __name__ == '__main__':
    ot = "abcdefghijklmnopqrstuvwxyz"
    posun = 2
    st_prosty_posun = prosty_posun(ot, posun)
    print("Prosty posun: "+st_prosty_posun)
    a = 17
    b = 3
    st_afinni_sifra = afinni_sifra_encryption(ot, a, b)
    st_afinni_sifra_decryption = afinni_sifra_decrypt(ot, a, b)
    print("Afinni sifra encryption: "+st_afinni_sifra)
    print("Afinni sifra decryption: " + st_afinni_sifra_decryption)

    ot_transpozice = "abcdefghijklmnopqrstuvwxyzacbr"
    sloupce = 5
    radky = 6
    st_transpozice = transpozicni_sifra(ot_transpozice, radky, sloupce)
    print("Transpozicni sifra encryption: " + st_transpozice)